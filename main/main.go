package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
)

func client() {
	udpRetAddr, err := net.ResolveUDPAddr("udp4", "127.0.0.1:51819")
	udpRetConn, err := net.ListenUDP("udp4", udpRetAddr)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Listening on (UDP) %q \n", udpRetAddr)
	defer udpRetConn.Close()

	tcpRemoteAddr, err := net.ResolveTCPAddr("tcp4", "145.100.180.215:3333")
	tcpRemoteConn, err := net.DialTCP("tcp4", nil, tcpRemoteAddr)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Dialed (TCP) %q \n", tcpRemoteConn.RemoteAddr())
	defer tcpRemoteConn.Close()

	buf := make([]byte, 2000)
	for {
		n, addr, err := udpRetConn.ReadFromUDP(buf)
		if err != nil {
			log.Fatal(err)
		} else if err == nil && n > 0 {
			fmt.Printf("%d bytes read from %q \n", n, addr)
			k, err := tcpRemoteConn.Write(buf[:n])
			if err != nil {
				log.Fatal(err)
			} else {
				fmt.Printf("%d bytes sent to %q \n", k, tcpRemoteConn.RemoteAddr())
			}
		}
	}
}

func server() {
	tcpPortAddr, err := net.ResolveTCPAddr("tcp4", ":3333")
	tcpPortListener, err := net.ListenTCP("tcp4", tcpPortAddr)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Listening on %q \n", tcpPortAddr)
	defer tcpPortListener.Close()

	udpWGAddr, err := net.ResolveUDPAddr("udp4", "127.0.0.1:51820")
	//udpWGLocAddr, err := net.ResolveUDPAddr("udp4", "127.0.0.1:51819")
	udpWGConn, err := net.DialUDP("udp4", nil, udpWGAddr)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Dialed WG-Client (UDP) %q \n", udpWGConn.RemoteAddr())
	defer udpWGConn.Close()

	udpLocAddr, err := net.ResolveUDPAddr("udp4", "127.0.0.1:51819")
	udpLocConn, err := net.ListenUDP("udp4", udpLocAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer udpLocConn.Close()

	buf2 := make([]byte, 2000)
	go func() {
		n, addr, err := udpLocConn.ReadFromUDP(buf2)
		fmt.Println("I got something booooyys!!")
		if err != nil {
			log.Fatal(err)
		} else if err == nil && n > 0 {
			fmt.Printf("%d bytes read from %q", n, addr)
		}

	}()

	for {
		conn, err := tcpPortListener.AcceptTCP()
		if err != nil {
			log.Fatal(err)
		}
		buf := make([]byte, 2000)
		n, err := conn.Read(buf)
		fmt.Printf("%d bytes read from remote WG-TCP %q \n", n, conn.RemoteAddr())
		if err != nil {
			log.Fatal(err)
		} else if err == nil && n > 0 {
			buf2 := make([]byte, 2000)
			udpWGConn.WriteToUDP(buf[:n], udpWGAddr)
			fmt.Printf("%d bytes written to local WG-Client %q \n", n, udpWGConn.RemoteAddr())
			l, err := bufio.NewReader(udpWGConn).Read(buf2)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%d bytes read from WG-Client", l)
		}
	}

}

// Mode checks the mode argument given when launching the application
func Mode() int {
	if len(os.Args) < 1 {
		return 0
	}
	var modeArg = os.Args[1]
	if modeArg == "-c" || modeArg == "--client" {
		return 1
	} else if modeArg == "-s" || modeArg == "--server" {
		return 2
	}
	return 0
}

func main() {
	switch Mode() {
	case 1: // Client
		client()
	case 2: // Server
		server()
	case 0: // Error
		log.Fatal("The given operating mode is not correct, use: --client (-c), --server (-s) or --peertopeer (-p) \n")
	}
}
